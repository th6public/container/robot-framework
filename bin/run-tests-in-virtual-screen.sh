#!/bin/sh

HOME=/opt/robotframework/temp

if [ "${ROBOT_TEST_RUN_ID}" = "" ]
then
    ROBOT_REPORTS_FINAL_DIR="/opt/robotframework/reports"
else
    REPORTS_DIR_HAS_TRAILING_SLASH=`echo /opt/robotframework/reports | grep '/$'`

    if [ ${REPORTS_DIR_HAS_TRAILING_SLASH} -eq 0 ]
    then
        ROBOT_REPORTS_FINAL_DIR="/opt/robotframework/reports${ROBOT_TEST_RUN_ID}"
    else
        ROBOT_REPORTS_FINAL_DIR="/opt/robotframework/reports/${ROBOT_TEST_RUN_ID}"
    fi
fi

# Ensure the output folder exists
mkdir -p ${ROBOT_REPORTS_FINAL_DIR}

# No need for the overhead of Pabot if no parallelisation is required
if [ $ROBOT_THREADS -eq 1 ]
then
    xvfb-run \
        --server-args="-screen 0 ${SCREEN_WIDTH}x${SCREEN_HEIGHT}x${SCREEN_COLOUR_DEPTH} -ac" \
        robot \
        --outputDir $ROBOT_REPORTS_FINAL_DIR \
        ${ROBOT_OPTIONS} \
        $ROBOT_TESTS_DIR 
        #>> $ROBOT_REPORTS_FINAL_DIR/console.out
else
    xvfb-run \
        --server-args="-screen 0 ${SCREEN_WIDTH}x${SCREEN_HEIGHT}x${SCREEN_COLOUR_DEPTH} -ac" \
        pabot \
        --verbose \
        --processes $ROBOT_THREADS \
        ${PABOT_OPTIONS} \
        --outputDir $ROBOT_REPORTS_FINAL_DIR \
        ${ROBOT_OPTIONS} \
        $ROBOT_TESTS_DIR
fi

ROBOT_EXIT_CODE=$?

if [ ${AWS_UPLOAD_TO_S3} = true ]
then
    echo "Uploading report to AWS S3..."
    aws s3 sync $ROBOT_REPORTS_FINAL_DIR/ s3://${AWS_BUCKET_NAME}/robot-reports/
    echo "Reports have been successfully uploaded to AWS S3!"
fi

exit $ROBOT_EXIT_CODE
